# Socials and links
some are more active than others

## normal socials
- 🐞 [Cohost](https://cohost.org/meow-d)
- 🐘 [Mastodon](https://mas.to/@meow_d)
- 🐸 [Tumblr](https://www.tumblr.com/blog/meow-d)
- 🔥🤓🔥 [Reddit](https://www.reddit.com/user/meow_d_)

## technically socials
- 📺 [Anilist](https://anilist.co/user/meowd/)
- 🐙 [Github](https://github.com/meow-d)

## socials that i signed up just to try out but idk anything about it
- 🟥 [Raddle](https://raddle.me/user/meow_d)
- 🐨 [Lemmy](https://lemmy.blahaj.zone/u/meow_d)

## inactive
- 🔥🔥🐦🔥🔥 [Twitter](https://twitter.com/meow_dddaswe)